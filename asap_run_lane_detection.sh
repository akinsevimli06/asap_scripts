#!/bin/bash

dir_main="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
dir_app_launchers=${dir_main}"/app_launchers"
#echo ${dir_app_launchers}

array_path_scripts=()

array_path_scripts+=("${dir_app_launchers}/driver_applanix.sh")
array_path_scripts+=("${dir_app_launchers}/driver_cameras.sh")
array_path_scripts+=("${dir_app_launchers}/driver_can_bridge.sh")
array_path_scripts+=("${dir_app_launchers}/driver_can_control.sh")
array_path_scripts+=("${dir_app_launchers}/driver_can_speed_to_twist.sh")
array_path_scripts+=("${dir_app_launchers}/driver_gnss_pose_to_map.sh")
array_path_scripts+=("${dir_app_launchers}/driver_vlp16s.sh")
array_path_scripts+=("${dir_app_launchers}/driver_vls128.sh")
array_path_scripts+=("${dir_app_launchers}/perception_transformations_manager.sh")
array_path_scripts+=("${dir_app_launchers}/perception_lidar_stitcher.sh")
array_path_scripts+=("${dir_app_launchers}/perception_ground_remover.sh")
array_path_scripts+=("${dir_app_launchers}/lane_ds_and_cluster.sh")
array_path_scripts+=("${dir_app_launchers}/lane_detection_lanenet.sh")
array_path_scripts+=("${dir_app_launchers}/lane_detection_synth_ground_generator.sh")
array_path_scripts+=("${dir_app_launchers}/lane_detection_sliding_window_extractor.sh")
array_path_scripts+=("${dir_app_launchers}/localization_applanix2fix.sh")
array_path_scripts+=("${dir_app_launchers}/lane_detection_trajectory_generator.sh")
array_path_scripts+=("${dir_app_launchers}/lane_localization.sh")
array_path_scripts+=("${dir_app_launchers}/lane_planning.sh")
array_path_scripts+=("${dir_app_launchers}/lane_tracking.sh")
array_path_scripts+=("${dir_app_launchers}/lane_control.sh")

#array_path_scripts+=("${dir_app_launchers}/run_perspective_detections.sh")
array_path_scripts+=("${dir_app_launchers}/run_rviz_lane.sh")
array_path_scripts+=("${dir_app_launchers}/speedgoat_can_to_shutdown.sh")
array_path_scripts+=("${dir_app_launchers}/speedgoat_gps_pose_to_can.sh")


function run_in_tab() {
  cmd1="bash -c $1"
  mate-terminal --tab --command="bash -ic 'echo $cmd1  >> $HOME/.bash_history; trap bash SIGINT ;$cmd1 ; bash'"
}

run_in_tab "${dir_app_launchers}/roscore.sh"

sleep 2

# Run in separate tabs
for path_script in "${array_path_scripts[@]}"; do
  run_in_tab "${path_script}"
done
