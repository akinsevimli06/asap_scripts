#!/bin/bash

dir_main="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
dir_app_launchers=${dir_main}"/app_launchers"
#echo ${dir_app_launchers}

array_path_scripts=()
#array_path_scripts+=("${dir_app_launchers}/driver_gnss_pose_to_map.sh")
#array_path_scripts+=("${dir_app_launchers}/lane_control.sh")
array_path_scripts+=("${dir_app_launchers}/lane_localization.sh")
#array_path_scripts+=("${dir_app_launchers}/lane_planning.sh")
array_path_scripts+=("${dir_app_launchers}/lane_tracking.sh")
array_path_scripts+=("${dir_app_launchers}/localization_applanix2fix.sh")
array_path_scripts+=("${dir_app_launchers}/perception_ds_and_cluster.sh")
array_path_scripts+=("${dir_app_launchers}/perception_transformations_manager.sh")
array_path_scripts+=("${dir_app_launchers}/lane_detection_synth_ground_generator.sh")
array_path_scripts+=("${dir_app_launchers}/lane_detection_sliding_window_extractor.sh")
array_path_scripts+=("${dir_app_launchers}/lane_detection_trajectory_generator.sh")
array_path_scripts+=("${dir_app_launchers}/lane_detection_lanenet.sh")


array_path_scripts+=("${dir_app_launchers}/lane_republish.sh")


function run_in_tab() {
  cmd1="bash -c $1"
  mate-terminal --tab --command="bash -ic 'echo $cmd1  >> $HOME/.bash_history; trap bash SIGINT ;$cmd1 ; bash'"
}

#run_in_tab "${dir_app_launchers}/roscore.sh"

sleep 2

# Run in separate tabs
for path_script in "${array_path_scripts[@]}"; do
  run_in_tab "${path_script}"
done
