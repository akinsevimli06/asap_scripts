#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash
rqt_plot /current_velocity/twist/linear/x /ctrl_cmd/cmd/linear_velocity
