#!/bin/bash

ifmetric enp66s0 9001
ifmetric enp65s0 9001
ifmetric enp64s0 9001
ifmetric enp63s0 9001

ifmetric enp40s0 9001
ifmetric enp39s0 9001
ifmetric enp38s0 9001
ifmetric enp37s0 9001

ifmetric enp29s0 9001
ifmetric enp30s0 9001
ifmetric enp31s0 9001
ifmetric enp28s0 9001

ifmetric enp69s0 9001
ifmetric enp70s0 1
