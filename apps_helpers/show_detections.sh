#!/bin/bash

rqt_image_view /visualize_classifications_2d/cam_fm_01/image_raw &
rqt_image_view /visualize_classifications_2d/cam_fl_01/image_raw &
rqt_image_view /visualize_detections_2d/cam_fl_02/image_raw &
rqt_image_view /visualize_detections_2d/cam_fl_03/image_raw &
rqt_image_view /visualize_classifications_2d/cam_fr_01/image_raw &
rqt_image_view /visualize_classifications_2d/cam_fr_02/image_raw &
rqt_image_view /visualize_detections_2d/cam_fr_03/image_raw &
rqt_image_view /visualize_detections_2d/cam_bm_01/image_raw &