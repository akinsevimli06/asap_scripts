#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash

function run_in_tab() {
  cmd1="bash -c $1"
  mate-terminal --tab --command="bash -ic 'echo $cmd1  >> $HOME/.bash_history; trap bash SIGINT ;$cmd1 ; bash'"
}

run_in_tab "/home/user/projects/asap_scripts/app_launchers/planning_planner.sh"
run_in_tab "/home/user/projects/asap_scripts/app_launchers/planning_control.sh"