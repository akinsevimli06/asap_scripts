#!/bin/bash
#ROS exportations 
#ROS source 

#for reverse motion
source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash
roslaunch park_manager forward_motion.launch

#for starting parking launch
roslaunch ~/projects/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/parking/bmc_parking_forward.launch
