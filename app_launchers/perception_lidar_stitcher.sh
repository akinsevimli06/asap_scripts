#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects_akin/asap_ws/devel/setup.bash
roslaunch ~/projects_akin/asap_ws/src/lidar_stitcher/launch/lidar_stitcher.launch
roslaunch lidar_stitcher lidar_stitcher.launch
