#!/bin/bash

SLEEP_SECONDS=4
echo "Sleep for ground remover for $SLEEP_SECONDS."
sleep $SLEEP_SECONDS
echo "Sleep for ground remover is over, starting it..."

source /opt/ros/melodic/setup.bash
source ~/projects_akin/asap_ws/devel/setup.bash
roslaunch linefit_ground_segmentation_ros segmentation.launch
