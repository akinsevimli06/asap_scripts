#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects_akin/asap_autoware_ws/install/setup.bash
rosrun rviz rviz -d ~/projects/asap_scripts/rviz_configurations/asap_lane_detection.rviz --name lane_detection
