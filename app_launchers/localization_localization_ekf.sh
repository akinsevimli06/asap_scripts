#!/bin/bash
SLEEP_SECONDS=5
echo "Sleep for localization for $SLEEP_SECONDS."
sleep $SLEEP_SECONDS
echo "Sleep for localization ekf is over, starting it..."
source /opt/ros/melodic/setup.bash
source ~/projects_akin/asap_autoware_ws/install/setup.bash
roslaunch ~/projects_akin/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/general/localization_ekf.launch
