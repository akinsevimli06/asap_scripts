#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects_akin/asap_autoware_ws/install/setup.bash
rosrun rviz rviz -d ~/projects_akin/asap_scripts/rviz_configurations/asap_default.rviz --name rviz_asap_main
