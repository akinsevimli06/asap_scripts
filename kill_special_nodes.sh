#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash

array_nodes_to_kill=()

rviz_to_kill=()

#perception_ground_remover.sh (old)
array_nodes_to_kill+=("/detection_node")

#perception_ground_remover.sh (new)
array_nodes_to_kill+=("/segmenter")

#lane_detection_lanenet.sh
rviz_to_kill+=("/lanenet_ros")

#lane_detection_synth_ground_generator.sh
array_nodes_to_kill+=("/synth_ground_generator_node")

#lane_detection_sliding_window_extractor.sh
array_nodes_to_kill+=("/sliding_window_extractor_node")

#lane_detection_trajectory_generator.sh
array_nodes_to_kill+=("/trajectory_generator_node")

#lane_tracking.sh
array_nodes_to_kill+=("/lidar_kf_contour_track")

#run_rviz_lane.sh !
rviz_to_kill+=("/rviz")

#lane_planning.sh
array_nodes_to_kill+=("/op_lane_follower")

#lane_control.sh
array_nodes_to_kill+=("/pure_pursuit")
array_nodes_to_kill+=("/twist_filter")
array_nodes_to_kill+=("/twist_gate")

#perception_detector_2d_thermal.sh

array_nodes_to_kill+=("/thermal_2d_detection_node")


#planning_global_planner.sh
array_nodes_to_kill+=("/op_global_planner")

#/planning_planner.sh
array_nodes_to_kill+=("/lidar_kf_contour_track")
array_nodes_to_kill+=("/op_behavior_selector")
array_nodes_to_kill+=("/op_common_params")
array_nodes_to_kill+=("/op_motion_predictor")
array_nodes_to_kill+=("/op_trajectory_evaluator")
array_nodes_to_kill+=("/op_trajectory_generator")
array_nodes_to_kill+=("/waypoint_marker_publisher")


#planning_control.sh - PREV KILLED

# ----  ARAZİ MODE  ---- - PREV KILLED ALL NODES

# ----  FREESPACE MODE  ----

#planning_ref_path_planner.sh
array_nodes_to_kill+=("/freespace_ref_path_planner")
#planning_planner_free.sh - PREV KILLED
#planning_control_free.sh-  PREV KILLED



function kill_node_rviz() {
   rosnode list | grep  $1 | while read -r line ; do
    echo "$line is alive, will proceed to kill it."
    rosnode kill $line
  done
}

function kill_node() {
  while rosnode list | grep -q $1; do
    echo "$1 is alive, will proceed to kill it."
    rosnode kill $1
  done
}




for node_to_kill in "${array_nodes_to_kill[@]}"; do
  kill_node "${node_to_kill}"
done


for node_to_kill in "${rviz_to_kill[@]}"; do
  kill_node_rviz "${node_to_kill}"
done

echo "Killed special nodes."