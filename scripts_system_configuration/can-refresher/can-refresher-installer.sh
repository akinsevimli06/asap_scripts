#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo $DIR

chmod +x $DIR/can-refresher.sh
chmod 644 $DIR/can-refresher.service

cp $DIR/can-refresher.sh /usr/local/bin/
cp $DIR/can-refresher.service /etc/systemd/system/

systemctl enable can-refresher.service

echo "Installation is complete."
