#!/bin/bash

for i in {0..6}
do
  ip link set down can${i}
  ip link set can${i} type can bitrate 500000
  ip link set up can${i}
done


ifmetric enp63s0 9001
ifmetric enp64s0 9001
ifmetric enp65s0 9001
ifmetric enp66s0 9001

ifmetric enp37s0 9001
ifmetric enp38s0 9001
ifmetric enp39s0 9001
ifmetric enp40s0 9001

ifmetric enp28s0 9001
ifmetric enp29s0 9001
ifmetric enp30s0 9001
ifmetric enp31s0 9001

ifmetric enp69s0 9001
ifmetric enp70s0 9001
ifmetric enp71s0 1