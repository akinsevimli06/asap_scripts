#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash

while rosnode list | grep -q /op_global_planner
do
   echo "op_global_planner is alive, will proceed to kill it.";
   rosnode kill /op_global_planner
done

echo "op_global_planner is dead, will relaunch it.";

roslaunch ~/projects/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/general/global_planner.launch &

