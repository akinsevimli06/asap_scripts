#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash

while rosnode list | grep -q /freespace_ref_path_planner
do
   echo "freespace_ref_path_planner is alive, will proceed to kill it.";
   rosnode kill /freespace_ref_path_planner
done

echo "freespace_ref_path_planner is dead, will relaunch it.";

~/projects/asap_scripts/app_launchers/planning_ref_path_planner.sh &

