#!/bin/bash

~/projects/asap_scripts/interactive_scripts/kill_lp_c.sh
~/projects/asap_scripts/interactive_scripts/restart_ref_freespace.sh

function wait_till_topic_sends_1() {
  echo Waiting till $2 is ready started
  while [ true ]; do
    if [ "$(rostopic echo $1 -n 1 | head -n 1)" -eq 1 ]; then
      echo "111"
      break
    else
      echo Waiting till $2 is ready
    fi
    sleep 0.1
  done
  echo $2 is ready
}

wait_till_topic_sends_1 "/software_health_checker/is_functional/freespace_ref_path_planner/data" "Freespace Ref Planner"