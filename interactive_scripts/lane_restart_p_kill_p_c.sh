#!/bin/bash
source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash

array_nodes_to_kill=()
array_nodes_to_kill+=("/op_lane_follower")

#lane_control.sh
array_nodes_to_kill+=("/pure_pursuit")
array_nodes_to_kill+=("/twist_filter")
array_nodes_to_kill+=("/twist_gate")

function kill_node() {
  while rosnode list | grep -q $1; do
    echo "$1 is alive, will proceed to kill it."
    rosnode kill $1
  done
}

for node_to_kill in "${array_nodes_to_kill[@]}"; do
  kill_node "${node_to_kill}"
done

echo "Killed local_planner and controller"



~/projects/asap_scripts/app_launchers/lane_planning.sh &



function wait_till_topic_sends_1() {
  echo Waiting till $2 is ready started
  while [ true ]; do
    if [ "$(rostopic echo $1 -n 1 | head -n 1)" -eq 1 ]; then
      echo "111"
      break
    else
      echo Waiting till $2 is ready
    fi
    sleep 0.1
  done
  echo $2 is ready
}

wait_till_topic_sends_1 "/software_health_checker/is_functional/op_lane_follower/data" "Lane Follower"


