#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash

echo "params: "
echo "1) easting: $1"
echo "2) northing: $2"
echo "3) altitude: $3"
echo "4) pcds_folder: $4"
echo "5) vector_map_folder: $5"
echo "6) use_ndt: $6"

# Kill the nodes
array_nodes_to_kill=()
array_nodes_to_kill+=("/vector_map_loader")
array_nodes_to_kill+=("/points_map_loader")
array_nodes_to_kill+=("/rostopic_pub_map_origin")
array_nodes_to_kill+=("/ndt_matching")
array_nodes_to_kill+=("/ekf_localizer")

function kill_node() {
  while rosnode list | grep -q $1; do
    echo "$1 is alive, will proceed to kill it."
    rosnode kill $1
  done
}

for node_to_kill in "${array_nodes_to_kill[@]}"; do
  kill_node "${node_to_kill}"
done

# Relaunch them

roslaunch ~/projects/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/general/map.launch pcds_folder:="$4" vector_map_folder:="$5" &

roslaunch ~/projects/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/general/localization_origin_pub.launch map_easting:="$1" map_northing:="$2" map_altitude:="$3" &

sleep 3

roslaunch ~/projects/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/general/localization_ekf.launch &

if [ $6 = "true" ] ; then

roslaunch ~/projects/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/general/localization_ndt.launch &

fi

