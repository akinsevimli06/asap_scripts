#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash
source ~/projects/asap_lane_detection_ws/devel/setup.bash

# Kill the nodes
array_nodes_to_kill=()

#Kill map
array_nodes_to_kill+=("/vector_map_loader")
array_nodes_to_kill+=("/points_map_loader")

#Kill localization utils.
array_nodes_to_kill+=("/rostopic_pub_map_origin")
array_nodes_to_kill+=("/ndt_matching")
array_nodes_to_kill+=("/ekf_localizer")

#Kill localization constant
array_nodes_to_kill+=("/base_link_to_velodyne")
array_nodes_to_kill+=("/ndt_pose_to_ndt_vel")
array_nodes_to_kill+=("/ndt_pose_to_ndt_vel")
array_nodes_to_kill+=("/ekf_pose_to_base_link")
array_nodes_to_kill+=("/world_to_map")
array_nodes_to_kill+=("/map_to_mobility")
array_nodes_to_kill+=("/vel_pose_connect")

#Kill op planner utils.
array_nodes_to_kill+=("/lidar_kf_contour_track")
array_nodes_to_kill+=("/op_behavior_selector")
array_nodes_to_kill+=("/op_common_params")
array_nodes_to_kill+=("/op_motion_predictor")
array_nodes_to_kill+=("/op_trajectory_evaluator")
array_nodes_to_kill+=("/op_trajectory_generator")
array_nodes_to_kill+=("/waypoint_marker_publisher")

#Kill control utils.
array_nodes_to_kill+=("/pure_pursuit")
array_nodes_to_kill+=("/twist_filter")

array_nodes_to_kill+=("/thermal_2d_detection_node")

array_nodes_to_kill+=("/rviz")

function kill_node() {
   rosnode list | grep  $1 | while read -r line ; do
    echo "$line is alive, will proceed to kill it."
    rosnode kill $line
  done
}
for node_to_kill in "${array_nodes_to_kill[@]}"; do
  kill_node "${node_to_kill}"
done

~/projects/asap_scripts/app_launchers/lane_detection_lanenet.sh &
~/projects/asap_scripts/app_launchers/lane_detection_synth_ground_generator.sh &
~/projects/asap_scripts/app_launchers/lane_detection_sliding_window_extractor.sh &

~/projects/asap_scripts/app_launchers/lane_detection_trajectory_generator.sh &

roslaunch ~/projects/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/lane_detection/localization.launch &
roslaunch ~/projects/asap_autoware_ws/src/asap_autoware_data/Pinarbasi-01/my_launch/lane_detection/tracking.launch &

~/projects/asap_scripts/app_launchers/lane_detection_rviz.sh &
