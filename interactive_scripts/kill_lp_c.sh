#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash

array_nodes_to_kill=()
array_nodes_to_kill+=("/pure_pursuit")
array_nodes_to_kill+=("/twist_filter")
array_nodes_to_kill+=("/lidar_kf_contour_track")
array_nodes_to_kill+=("/op_behavior_selector")
array_nodes_to_kill+=("/op_common_params")
array_nodes_to_kill+=("/op_motion_predictor")
array_nodes_to_kill+=("/op_trajectory_evaluator")
array_nodes_to_kill+=("/op_trajectory_generator")
array_nodes_to_kill+=("/waypoint_marker_publisher")

function kill_node() {
  while rosnode list | grep -q $1; do
    echo "$1 is alive, will proceed to kill it."
    rosnode kill $1
  done
}

for node_to_kill in "${array_nodes_to_kill[@]}"; do
  kill_node "${node_to_kill}"
done

echo "Killed local_planner, controller"