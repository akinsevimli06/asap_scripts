#!/bin/bash

echo "params: "
# Standard - Offroad
echo "1) Driving Mode: $1"

function wait_till_topic_sends_1() {
  echo Waiting till $2 is ready started
  while [ true ]; do
    if [ "$(rostopic echo $1 -n 1 | head -n 1)" -eq 1 ]; then
      echo "111"
      break
    else
      echo Waiting till $2 is ready
    fi
    sleep 0.1
  done
  echo $2 is ready
}

~/projects/asap_scripts/interactive_scripts/kill_lp_c.sh

if [ "$1" = "Standard" ] || [ "$1" = "Offroad" ]; then

  wait_till_topic_sends_1 "/planners_health_checker_node/is_functional/global_planner/data" "Global Planner"

  if [ "$1" = "Offroad" ]; then
    echo "Driving Mode: Offroad"
    ~/projects/asap_scripts/app_launchers/planning_planner_dirt.sh &
  else
    echo "Driving Mode: Standard"
    ~/projects/asap_scripts/app_launchers/planning_planner.sh &
  fi

  wait_till_topic_sends_1 "/planners_health_checker_node/is_functional/local_planner/data" "Local Planner"

  ~/projects/asap_scripts/app_launchers/planning_control.sh &

else

  wait_till_topic_sends_1 "/software_health_checker/is_functional/freespace_ref_path_planner/data" "Freespace Ref Planner"

  echo "Driving Mode: Freespace"

  ~/projects/asap_scripts/app_launchers/planning_planner_free.sh &

  wait_till_topic_sends_1 "/planners_health_checker_node/is_functional/local_planner/data" "Local Planner"

  ~/projects/asap_scripts/app_launchers/planning_control_free.sh &

fi




#wait_till_topic_sends_1 "/planners_health_checker_node/is_functional/controller/data" "Controller"
