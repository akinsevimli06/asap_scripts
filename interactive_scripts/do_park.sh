#!/bin/bash

echo "params: "

echo "1) Latitude: $1"
echo "2) Longitude: $2"

/home/user/projects/asap_scripts/interactive_scripts/restart_gp.sh

function wait_till_topic_sends_1() {
  echo Waiting till $2 is ready started
  while [ true ]; do
    if [ "$(rostopic echo $1 -n 1 | head -n 1)" -eq 1 ]; then
      echo "111"
      break
    else
      echo Waiting till $2 is ready
    fi
    sleep 0.1
  done
  echo $2 is ready
}

wait_till_topic_sends_1 "/planners_health_checker_node/is_functional/global_planner/data" "Global Planner"


rostopic pub -1 /position_request sensor_msgs/NavSatFix -- "{header: {stamp: now, frame_id: world}, latitude: $1, longitude: $2}"

