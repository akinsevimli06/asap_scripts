#!/bin/bash

source /opt/ros/melodic/setup.bash
source ~/projects/asap_autoware_ws/install/setup.bash
source ~/projects/asap_lane_detection_ws/devel/setup.bash

# Kill the nodes
array_nodes_to_kill=()

array_nodes_to_kill+=("/lanenet_ros")

#sliding_window_extractor
array_nodes_to_kill+=("/sliding_window_extractor")
#
##trajectory_generator
array_nodes_to_kill+=("/trajectory_generator")

array_nodes_to_kill+=("/synth_ground_extractor")
#
##localization
array_nodes_to_kill+=("/rostopic_pub_map_origin")
array_nodes_to_kill+=("/vel_pose_connect")
array_nodes_to_kill+=("/world_to_map")
array_nodes_to_kill+=("/map_to_mobility")
array_nodes_to_kill+=("/base_link_gnss_to_base_link")
array_nodes_to_kill+=("/base_link_to_velodyne")
#
###tracking
#array_nodes_to_kill+=("/lidar_kf_contour_track")

#rviz
array_nodes_to_kill+=("/rviz")

#planning


function kill_node() {
   rosnode list | grep  $1 | while read -r line ; do
    echo "$line is alive, will proceed to kill it."
    rosnode kill $line
  done
}

for node_to_kill in "${array_nodes_to_kill[@]}"; do
  kill_node "${node_to_kill}"
done

~/projects/asap_scripts/app_launchers/run_rviz_asap.sh &
~/projects/asap_scripts/app_launchers/localization_localization.sh &
~/projects/asap_scripts/app_launchers/localization_localization_constants.sh &
~/projects/asap_scripts/app_launchers/localization_map.sh &
~/projects/asap_scripts/app_launchers/perception_detector_2d_thermal.sh &


