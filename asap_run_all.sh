#!/bin/bash

dir_main="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
dir_app_launchers=${dir_main}"/app_launchers"
#echo ${dir_app_launchers}

array_path_scripts=()
#array_path_scripts+=("${dir_app_launchers}/driver_applanix.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_cameras.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_can_bridge.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_can_control.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_can_speed_to_twist.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_gnss_pose_to_map.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_radars.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_cameras_thermal.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_vlp16s.sh")
#array_path_scripts+=("${dir_app_launchers}/driver_vls128.sh")
array_path_scripts+=("${dir_app_launchers}/health_checker_planners.sh")
array_path_scripts+=("${dir_app_launchers}/health_checker_sensors.sh")
array_path_scripts+=("${dir_app_launchers}/localization_applanix2fix.sh")
array_path_scripts+=("${dir_app_launchers}/perception_classifier_2d.sh")
array_path_scripts+=("${dir_app_launchers}/perception_detector_2d.sh")
array_path_scripts+=("${dir_app_launchers}/perception_detector_2d_thermal.sh")
array_path_scripts+=("${dir_app_launchers}/perception_ds_and_cluster.sh")
array_path_scripts+=("${dir_app_launchers}/perception_frustum_projector.sh")
#array_path_scripts+=("${dir_app_launchers}/perception_ground_remover.sh") # Old one
array_path_scripts+=("${dir_app_launchers}/perception_ground_remover_traffic.sh")
#array_path_scripts+=("${dir_app_launchers}/perception_lidar_stitcher.sh")
array_path_scripts+=("${dir_app_launchers}/perception_radar_classification.sh")
array_path_scripts+=("${dir_app_launchers}/perception_radar_stitcher.sh")
#array_path_scripts+=("${dir_app_launchers}/perception_tl_handler.sh")
array_path_scripts+=("${dir_app_launchers}/perception_transformations_manager.sh")
#array_path_scripts+=("${dir_app_launchers}/perception_ts_handler.sh")
#array_path_scripts+=("${dir_app_launchers}/perception_visualize_detections_2d.sh")
array_path_scripts+=("${dir_app_launchers}/planning_parker.sh")
#array_path_scripts+=("${dir_app_launchers}/run_autoware.sh")
array_path_scripts+=("${dir_app_launchers}/run_perspective_detections.sh")
array_path_scripts+=("${dir_app_launchers}/run_perspective_detections_thermal.sh")
#array_path_scripts+=("${dir_app_launchers}/run_rtknavi.sh")
array_path_scripts+=("${dir_app_launchers}/run_rviz_asap.sh")
array_path_scripts+=("${dir_app_launchers}/software_health_checker.sh")
#array_path_scripts+=("${dir_app_launchers}/speedgoat_can_ref_to_pose.sh")
#array_path_scripts+=("${dir_app_launchers}/speedgoat_can_to_shutdown.sh")
#array_path_scripts+=("${dir_app_launchers}/speedgoat_can_to_diagnostic.sh")
#array_path_scripts+=("${dir_app_launchers}/speedgoat_gps_pose_to_can.sh")
array_path_scripts+=("${dir_app_launchers}/localization_localization_origin_pub.sh")
array_path_scripts+=("${dir_app_launchers}/localization_localization_ekf.sh")
array_path_scripts+=("${dir_app_launchers}/localization_localization_ndt.sh")
array_path_scripts+=("${dir_app_launchers}/localization_localization_constants.sh")
array_path_scripts+=("${dir_app_launchers}/localization_map.sh")
array_path_scripts+=("${dir_app_launchers}/localization_slam.sh")
array_path_scripts+=("${dir_app_launchers}/localization_slam_trans_manager.sh")
array_path_scripts+=("${dir_app_launchers}/localization_visual_diag.sh")

function run_in_tab() {
  cmd1="bash -c $1"
  gnome-terminal --tab --command="bash -ic 'echo $cmd1  >> $HOME/.bash_history; trap bash SIGINT ;$cmd1 ; bash'"
}

run_in_tab "${dir_app_launchers}/roscore.sh"

sleep 2

# Run in separate tabs
for path_script in "${array_path_scripts[@]}"; do
  run_in_tab "${path_script}"
done
