#!/bin/bash

# Orchestration:
# asap_run_all.sh launches
# asap_amazon_detections.perspective and asap_amazon_detections_thermal.perspective rqt files
# and rviz_asap_main rviz screen.
# these 3 files are tied to ws1 from the i3 config.
# Then, we sleep and switch to the ws1, run the navigation bar, move it up,
# set the rviz window to become tabbed, focus on rviz window,
# run the navigation_hmi and the diagnostics screen and focus on the navigation_hmi

dir_main="~/projects/asap_scripts"
dir_app_launchers=${dir_main}"/app_launchers"
#echo ${dir_app_launchers}

#sudo ~/projects/asap_scripts/scripts_system_configuration/can_refresh.sh

#~/projects/asap_scripts/scripts_system_configuration/flir_camera_ip_configuration.sh

# Launch Backend Scripts and Autonomous Driving Screen
sleep 3

i3-msg 'workspace 10; exec mate-terminal -- bash -c '~/projects/asap_scripts/asap_run_all.sh' --name asap_run_all_terminal &'

sleep 11

i3-msg 'workspace 1'

# Launch Navigation Bar

source /opt/ros/melodic/setup.bash
source ~/projects/navigation_hmi_ws/devel/setup.bash
~/projects/asap_navigation_bar/cmake-build-debug/asap_navigation_bar --name asap_navigation_bar &

# Launch Detections Screen
###
# WM_CLASS(STRING) = "asap_goal_selection_screen", "navigation_hmi"
#
# WM_CLASS(STRING) = "asap_navigation_bar", "asap_navigation_bar"
#
# WM_CLASS(STRING) = "rviz_asap_main", "rviz"
#
# WM_CLASS(STRING) = "asap_diagnostics_screen", "diagnostics"

sleep 1

i3-msg [class="asap_navigation_bar" instance="asap_navigation_bar"] move up, move up

i3-msg [class="rviz" instance="rviz_asap_main"] layout tabbed
i3-msg [class="rviz" instance="rviz_asap_main"] focus


# Launch Goal Selection Screen
/home/user/projects/navigation_hmi_ws/devel/lib/navigation_hmi/navigation_hmi /home/user/projects/navigation_hmi_ws/config/hmi.yaml --name asap_goal_selection_screen &

i3-msg [class="asap_navigation_bar" instance="asap_navigation_bar"] resize set height 78 px

/home/user/projects/diagnostics_hmi/devel/lib/diagnostics/diagnostics /home/user/projects/diagnostics_hmi/config/diagnostics.yaml --name asap_diagnostics_screen &

sleep 1

i3-msg [class="navigation_hmi" instance="asap_goal_selection_screen"] focus

while true;
do
sleep 5
done













